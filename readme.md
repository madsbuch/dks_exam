# Øveplan
## Vigtigt
* Approx
* Alle de første propositioner

## Definitiner
* Church Turing Thesis
* Stand in languages 
* Stand in for OPT
* Alle problemer

## Lemmaer
* Lemma 8
* Lemma 9

## Reduktioner
* 3SAT $\leq$ H.P. (3)
* H.P. $\leq$ TSP (3)
* 3SAT $\leq$ Tripartite Mathing (4)
* Tripartite Mathing $\leq$ Exact Cover by 3 sets (4)
* SAT $\leq$ NAESAT (1, 2)
* NAESAT $\leq$ 3-COLOR (3)
* 3SAT $\leq$ Independent Set (3)
* Hav en intuition om resten

## Algoritmer
* Approx Knapsack (5)
* Approx Euklid TSP (5)
* Generelt TSP (5)
* Approx Vertex Cover
* Johnson
* Christofides

\newpage

# P, NP and NPC.
* def __P__, __NP__, __NP-Hard__, __NPC__, Decision Problem, Church Turing Thesis, reduction
* Nogle lemmaer
* Reduktion
* Cooks theorem ved CSAT $\in$ NPC og CSAT $\leq$ SAT

\newpage

# Cook's theorem and the complexity of variants of SAT.
* def __P__, __NP__, __NP-Hard__, __NPC__, Decision Problem,
  Church Turing Thesis,reduction
* Lemma 9 (Pol TM som C-SAT)
* CSAT $\in$ NPC
* CSAT $\leq$ SAT

\newpage

# NP-complete graph problems.
* Intro, Reduktioner
* Ham Path $\in$ NPC
* TSP $\in$ NPC
* Independant set $\in$ NPC

1. Introducer: definer NP, NPC
2. skriv reductioner der skal laves ned: 3SAT $\leq$ Ham. Path $\leq$ TSP
3. forklar at da problemerne er i NP, kan de reduceres til SAT (via Cooks theorem)
   Og at vi vil reducere fra et NPC problem for at vise at alle NP problemer reduceres til dem
4. 3SAT $\leq$ Ham. Path
5. Ham. Path $\leq$ TSP
6. Independant set $\in$ NPC
7. Tak fordi i gad at lytte''

\newpage

# NP-complete problems involving sets and numbers.
* Tripartite Matching $\in$ NPC
* Exact covor by 3-sets $\in$ NPC
* Knapsack $\in$ NPC

Tripartite Matching $\leq$ ECB3S $\leq$ KNAPSACK (via Subset Sum)

Tripartite Matching $\leq$ Exact covor by 3-sets:
Exact covor by 3-sets er en generalisering af tripartite matching. Den er derfor
bevist. (skal formliseres)

\newpage

# Approximation algorithms.
* Intro: Approx ratio, 
* TSP - approx for euklidiske rum
* TSP - Christofides
* TSP - ingen generelt approx, 35.3 s. 1031
* 3SAT - Approx

Analysen er vigtig: Gennemgå lynhurtigt algoritmen, og brug tiden på analysen.

* PTAS  : Vi laver en (1+e) - approximation algorithm
* FPTAS : Også (1+e) - approximation algorithm, men denne stiger også polynomielt
  i størrelse af 1/e
* RP - randomized polynomial - onesided monte carlo. $\geq 1/2$ for at acceptere hvis
  en streng er i sproget. 0 for at acceptere hvis den ikke er i sproget.
* ZPP - Zero Probobalistic Polynomial - las vegas algorithm
* PP - Probobalistic Polynomial - two-sided monte-carlo med >1/2 chance for at
  ramme rigtigt (gør tingene x gange for at reducere fejlen til (1/2)^x)
* BPP - Bounded PP. Det samme som ovenfor. Blot 3/4 for at acceptere hvis strengen
  er i sproget og 1/4 for at acceptere hvis den ikke er.

## Extra Approx
* Heurostik: 2-, 3-, 4- OPT

Probobalistic method

\newpage

# Definitioner
* **P					** $L \in P \iff L = \{\forall x \in \{0, 1\}^*  : x $ afgøres i polonomiel tid $\}$
* **NP					** $L \in NP \iff \exists y \in \{0, 1\}^* : |y| \leq p(|x|) \land <y, x> \in L'$
* **NP-Hard				** $L \in \text{NP-Hard} \iff \forall L' \in NP : L' \leq L$
* **NPC					** $L \in NPC \iff L \in \text{NP-Hard} \land L \in NP$
* **Reduction			** $\forall x \in \{0, 1\}^* : x \in L_1 \iff r(x) \in L_2$
* **Stand-in decision	** $L_f = \{<x, b(j), y> : x \in \{1, 0\}^*, j \in \mathbb{N}, y \in \{1, 0\}, f(x)_j = y\}$
* **Stand-in OPT		** $L_{OPT} = \{x \in F : f(x) \geq x\}$ for $F=$feasible solutions

# Lemmas
* **Proposition 1  ** $\pi_1 \in P \Leftrightarrow \pi_2 \in P$
* **Proposition 2  ** noget med beviser
* **Proposition 3  ** Reduktioner er transitive
* **Proposition 4  ** hvis $L_1 \leq L_2 \land L_2 \in P \Rightarrow L_1 \in P$
* **Proposition 5  ** Lad $L \in NP-hard$. hvis $P \neq NP \Rightarrow L \not\in P$
* **Proposition 6  ** Lad $L \in NPC$. $P = NP \Leftrightarrow L \in P$
* **Proposition 7  ** $\forall L_1, L_2 : L_1 \in NP-hard \land L_1 \leq L_2 \Rightarrow L_2 \in NP-hard$ 
* **Lemma 8        ** $\forall f : \{0, 1\}^n \rightarrow \{0, 1\}^m : \exists C(\{0, 1\}^n) = f(\{0, 1\}^n)$
* **Lemma 9        ** Pol TM $\leq$ CSAT
* **Theorem 10     ** $SAT \in NPC$ (Cooks Theorem)
* **Theorem 11     ** $CSAT \in NPC$
* **Proposition 12 ** $CSAT \leq SAT$

\input{lemmas/lemma8}
\input{lemmas/lemma9}

# Alle problemer
\input{problems/sat}

## Graph problemer
* Clique
* Independent Sets
* Max Cut
* Max Bisection
* Bisection With
* Node Cover
* Hamiltonian path: Kan alle Verticer besøges
* Traveling salesman Problem
* Max Cut
* 3 Colouring

## Sets and numbers
* Tripartite  Matching
* Set Cover
* Set Packing
* Exact cover by 3-SAT
* Knapsack
* Binpacking

\input{problems/approx}

* 2SAT

# Vigtige Reduktioner
\input{reductions/csat_sat}
\input{reductions/sat_3sat}
\input{reductions/3sat_ind_set}
\input{reductions/3sat_ham_path}
\input{reductions/ham_path_tsp}

\input{reductions/3sat_tripartite_match}
\input{reductions/poly_tm_csat}

# Andre reduktioner
## Vertex Cover $\leq$ Set Cover
$ X = V$ and $F = \{S_w : w \in V\}$ where $S_w = \{(u, v) \in E : u=w\}$

# Terms
* Reduction 
* Polynomial time map 
* Decision Problem 
* Language 
* Encoding 
* Stand in languages 
* Stand in for OPT
* Injective maps
* Trekantsuligheden
* Literal
* Variabel
* Approimation Scheme: en parameteriseret approx algo
* fully polynomial Approimation Scheme: en parameteriseret approx algo der ikke
  stiger exponentielt

# Spørgsmål
* Lemma 8, hvorfor p(n) >= n ? kan vi ikke have sublinere algoritmer?
* hvor ange literals ift. clauses

![Hamiltonian](ham.jpg)


